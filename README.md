# ZeroTwoXCI
A homebrew application for the Nintendo Switch used to manage XCI titles.

DISCLAIMER: I take zero responsibility for any bans, damage, nuclear explosions, or anything else the use of ZeroTwoXCI may cause. Proceed with caution!

## Installation
1. Install FS sig patches, there are two ways of doing this:
    - Using the latest [Hekate](https://github.com/CTCaer/hekate) with the option ``kip1patch=nosigchk`` in ``hekate_ipl.ini``.
    - Using [ReiNX](https://github.com/Reisyukaku/ReiNX)
2. Place the zerotwoxci nro in the "switch" folder on your sd card, and run using the homebrew menu.

## Download
For the latest NRO go to [tags](https://gitlab.com/2168/zerotwoxci/tags).

## Usage
### For XCIs
1. Place XCI in ``/tinfoil/zerotwoxci/``
2. Put prod.keys keyfile on the root of your sd
3. Launch ZeroTwoXCI via the homebrew menu and install your XCIs.

## Donate
This is a donation to Adubbz (The creator of original tinfoil) and if you enjoy using this software and you have money to spare I recommend donating to him for the effort he has put into the original tinfoil.

[![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=TBB9Q9YL9AB74&lc=AU&item_name=Adubbz%27s%20Tools%20%26%20Game%20Mods&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)

## Credits
This project was developed by EliseZeroTwo.

I'd like to thank the following people for their help or for using some of their code.

Roothorick for Dedbae and keeping me sane

Adubbz for Tinfoil

SciresM for hactool code (specifically the hfs0 header functions)

The-4N for helping me

NullModel for a Hex To ASCII convirto https://github.com/ENCODE-DCC/kentUtils/commits?author=NullModel

Calypso for helping me to build for ARM and teaching me C 

My mother Alicia for testing and being a good mother, infact the best mother someone could ask for
