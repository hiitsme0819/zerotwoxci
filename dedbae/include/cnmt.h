#pragma once

#include <stdint.h>
#include <string.h>

#include "baecb.h"

typedef struct __attribute__((packed)) {
	uint64_t patchid;
	uint64_t min_sysversion;
} cnmt_application_header_t;

typedef struct __attribute__((packed)) {
	uint64_t original_tid;
	uint64_t min_sysversion;
} cnmt_patch_header_t;

typedef struct __attribute__((packed)) {
	uint64_t appid;
	uint64_t min_appversion;
} cnmt_addon_header_t;

typedef struct __attribute__((packed)) {
	uint64_t titleid;
	uint32_t version;
	uint8_t title_type;
	uint8_t unknown_0;
	uint16_t table_offset; // add 0x20 (for this header); this usually is 0x10 (meaning the table starts at 0x30)
	uint16_t content_count;
	uint16_t meta_count;
	char padding[12];
	union {
		cnmt_application_header_t app;
		cnmt_patch_header_t patch;
		cnmt_addon_header_t addon;
	};
} cnmt_header_t;

enum {
	CNMT_CONTENT_TYPE_META 			= 0,
	CNMT_CONTENT_TYPE_PROGRAM 		= 1,
	CNMT_CONTENT_TYPE_DATA 			= 2,
	CNMT_CONTENT_TYPE_CONTROL 		= 3,
	CNMT_CONTENT_TYPE_MANUAL 		= 4,
	CNMT_CONTENT_TYPE_LEGALINFO 	= 5,
	CNMT_CONTENT_TYPE_GAME_UPDATE	= 6,
};

char* cnmt_content_type_to_string(uint8_t type);

typedef struct __attribute__((packed)) {
	char hash[32];
	char ncaid[16]; // Always(?) the first half of the hash
	char size[6]; // 48(!) bit unsigned integer
	uint8_t type;
	char padding;
} cnmt_content_record_t;

// NOTE: Will discard the highest two bytes (think uint48_t)
static inline void cnmt_write_contentsize(uint64_t value, cnmt_content_record_t* in) {
	memcpy(in->size, &value, 6);
}

static inline uint64_t cnmt_read_contentsize(cnmt_content_record_t* in) {
	uint64_t out = 0;
	memcpy(&out, in->size, 6);
	return out;
}

// Only used for SystemUpdate
typedef struct __attribute__((packed)) {
	uint64_t titleid;
	uint32_t version;
	uint8_t type;
	char unknown_0; // From switchbrew wiki: "bit0 set = don't install?"
	char padding[2];
} cnmt_meta_record_t;

// HELP: After the content entries, there is a 32 byte "digest" value of which very little is known,
// other than it's a) probably specific to the application/version, and b) shared between gamecard and download
// versions of the application.
// Until someone figures out what the hell it is, we'll just copy the digest out of the original CNMT,
// or if that's not available, fill with zeroes.

// Translate an NCA content type to a CNMT content type
uint8_t cnmt_content_type_from_nca(uint8_t flag);

// Initialize a CNMT header with the defaults
// Optionally pass a header from an existing CNMT file to draw defaults from
void cnmt_init_header(cnmt_header_t* hd, cnmt_header_t* old_hd);

// Populate a CNMT content record for this file
void cnmt_create_content_record(cnmt_content_record_t* out, baecb_read in, void* cbdata);
