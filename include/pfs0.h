#pragma once

#include <stdint.h>

typedef struct __attribute__((packed))  {
    char magic[4];
    uint32_t filecount;
    uint32_t stringtable_size;
    char unknown_0[4];
} pfs0_header_t;

typedef struct __attribute__((packed)) {
    uint64_t offset;
    uint64_t filesize;
    uint32_t name_offset;
    char unknown_0[4];
} pfs0_file_entry_t;
